package com.ws3l;

import com.github.pagehelper.PageInfo;
import com.ws3l.model.VideoUserName;
import com.ws3l.pojo.Video;
import com.ws3l.service.impl.VideoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Lazy;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class VideoServiceTest {
    @Autowired
    VideoService videoService;

    @Test
    public void findHotVideo(){
        List<VideoUserName> list = videoService.findHotVideo();
        for (VideoUserName v : list){
            System.out.println(v);
        }

    }
    @Test
    public void findVideoById(){
        System.out.println(videoService.findVideoById(1));
    }

    @Test
    public void findVideoByClassify(){
        List<Integer> idList = new ArrayList<>();
        idList.add(1);
        List<VideoUserName> list = videoService.findVideoByClassify(3,idList);
        for (VideoUserName v : list){
            System.out.println(v);
        }
    }

    @Test
    public void addTags(){
        System.out.println(videoService.addTags(3));
    }
    @Test
    public void addCounts(){
        System.out.println(videoService.addCounts(3));
    }

    @Test
    public void findHotByClassify(){
        List<VideoUserName> list = videoService.findHotVideoByClassify(4,1);
        for (VideoUserName v : list){
            System.out.println(v);
        }
    }

    @Test
    public void findVideoClassify(){
        PageInfo<VideoUserName> list = videoService.findALLVideoByClassify(1,1,16);
        for (VideoUserName v : list.getList()){
            System.out.println(v);
        }
    }

    @Test
    public void findSearchVideo(){
        PageInfo<VideoUserName> list = videoService.findSearchVideo("游戏",1,8);
        for (VideoUserName v : list.getList()){
            System.out.println(v);
        }
    }

}

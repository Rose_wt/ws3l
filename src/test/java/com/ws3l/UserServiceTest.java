package com.ws3l;

import com.ws3l.pojo.User;
import com.ws3l.service.impl.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserServiceTest {
    @Autowired
    UserService userService;

    @Test
    public void userLogin(){
       User user= userService.userLogin("admin","123456");
        System.out.println(user);
    }
    @Test
    public void findByUserId(){
       User user= userService.findUserByUserId(3);
        System.out.println(user);
    }
    @Test
    public void findByUserName(){
        String s="admin";
       User user= userService.findUserByName(s);
        System.out.println(user);
    }
    @Test
    public void addUser(){
        User user=new User();
        user.setUserName("张三");
        user.setUserPassword("123456");
        user.setUserTel("15296351103");
        user.setUserEmail("321123@qq.com");
        user.setUserIcon("static/images/user/user3.jpg");
        userService.userSignUp(user);
    }
    @Test
    public  void updateUserPwdByEmail(){
        String userEmail="32612576812@qq.com";
        String userPwd="123456";
        int row=userService.updateUserPwdByEmail(userEmail,userPwd);
        System.out.println(row);
    }
    @Test
    public  void  updateUserByUser() {
     User user= userService.findUserByUserId(7);
     user.setUserName("bool2");
     user.setUserTel("17786924462");
      int row= userService.updateUserByUser(user);
        System.out.println(row);
    }


}

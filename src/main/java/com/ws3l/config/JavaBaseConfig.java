package com.ws3l.config;

import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JavaBaseConfig {
    @Bean
    //指定结果集的key，将结果存入redis缓存
    public KeyGenerator keyGenerator(){
        return new KeyGeneratorConfig();
    }
}

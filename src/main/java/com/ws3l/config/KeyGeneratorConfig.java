package com.ws3l.config;

import com.alibaba.fastjson.JSON;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.data.redis.core.script.DigestUtils;

import java.lang.reflect.Method;

public class KeyGeneratorConfig implements KeyGenerator {
    @Override
    public Object generate(Object target, Method method, Object... params) {
        StringBuilder sb = new StringBuilder();
        sb.append(target.getClass().getName());
        sb.append(method.getName());
        sb.append("&");
        for (Object obj : params) {
            if (obj != null){
                sb.append(obj.getClass().getName());
                sb.append("&");
            }
        }
        return sb;
    }
}

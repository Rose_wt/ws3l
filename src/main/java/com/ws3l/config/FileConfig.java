package com.ws3l.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;
import java.io.IOException;

/**
 * 配置静态资源映射
 */
@Configuration
public class FileConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry){
        File directory = new File("");//参数为空
        String filePath;
        try {
            filePath = directory.getCanonicalPath();
//            System.out.println(filePath);
            registry.addResourceHandler("/static/**").addResourceLocations("file:"+filePath+ File.separator+
                    "src"+File.separator+"main"+File.separator+"resources"+File.separator+"static"+File.separator);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("资源路径异常");
        }
    }
}

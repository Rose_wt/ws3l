package com.ws3l.service;

import com.ws3l.pojo.User;

public interface UserServices {
    public User userLogin(String userName,String userPwd);
    public User findUserByUserId(Integer id);
    public int userSignUp(User user);
    int updateUserPwdByEmail(String userEmail,String userPassword);
    int updateUserByUser(User user);
}

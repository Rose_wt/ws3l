package com.ws3l.service;

import com.ws3l.pojo.Member;

import java.util.List;

public interface MemberServices {
    List<Member> showAll();
}

package com.ws3l.service;

import com.github.pagehelper.PageInfo;
import com.ws3l.model.VideoUserName;
import com.ws3l.pojo.Video;

import java.util.HashMap;
import java.util.List;

public interface VideoServices {

    List<VideoUserName> findHotVideo();

    Video findVideoById(Integer videoId);

    List<VideoUserName> findVideoByClassify(Integer classify,List<Integer> idList);

    Integer addTags(Integer videoId);

    Integer addCounts(Integer videoId);

    Integer addVideo(Video video);

    List<VideoUserName> findHotVideoByClassify(Integer videoClassify, Integer videoId);

    PageInfo<VideoUserName> findALLVideoByClassify(Integer videoClassify,Integer pageNum,Integer pageSize);

    PageInfo<VideoUserName> findSearchVideo(String keyword,Integer pageNum,Integer pageSize);
    List<Video> findVideoByUserIdAndNominate(Integer videoUserId,Integer videoNominate);
}

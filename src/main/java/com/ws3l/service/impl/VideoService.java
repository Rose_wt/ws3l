package com.ws3l.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ws3l.mapper.VideoMapper;
import com.ws3l.model.VideoUserName;
import com.ws3l.pojo.Video;
import com.ws3l.service.VideoServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
@Service
public class VideoService implements VideoServices {
    @Autowired
    VideoMapper videoMapper;

    /**
     * 查询热门视频
     * @return 视频集合
     */
    @Cacheable(value = "videoUserName",keyGenerator = "keyGenerator")
    @Override
    public List<VideoUserName> findHotVideo() {
        return videoMapper.findHotVideo();
    }

    /**
     * 通过视频id查询视频
     * @param videoId 视频id
     * @return 视频
     */
    @Override
    public Video findVideoById(Integer videoId) {
        return videoMapper.findVideoById(videoId);
    }

    /**
     * 查询首页分类视频
     * @param classify,idList 视频分类：1为搞笑，2为舞蹈，3为科普，4为游戏
     * @return 视频集合
     */
    @Cacheable(value = "videoUserName",keyGenerator = "keyGenerator")
    @Override
    public List<VideoUserName> findVideoByClassify(Integer classify, List<Integer> idList) {
        return videoMapper.findVideoByClassify(classify,idList);
    }

    /**
     * 点赞
     * @param videoId 视频id
     * @return 影响行数
     */
    @Override
    public Integer addTags(Integer videoId) {
        return videoMapper.addTags(videoId);
    }

    /**
     * 增加播放量
     * @param videoId 视频id
     * @return 影响行数
     */
    @Override
    public Integer addCounts(Integer videoId) {
        return videoMapper.addCounts(videoId);
    }

    /**
     * 添加视频
     * @param video 视频
     * @return 影响行数
     */
    @Override
    public Integer addVideo(Video video) {
        return videoMapper.addVideo(video);
    }

    /**
     * 通过视频种类查询三个热门视频
     * @param videoClassify  视频分类
     * @return 热门视频
     */
    @Override
    public List<VideoUserName> findHotVideoByClassify(Integer videoClassify, Integer videoId) {
        return videoMapper.findHotVideoByClassify(videoClassify,videoId);
    }

    /**
     * 分页查询分类视频
     * @param videoClassify 视频类型
     * @return 视频集合
     */
    @Override
    public PageInfo<VideoUserName> findALLVideoByClassify(Integer videoClassify,Integer pageNum,Integer pageSize) {
        //使用分页插件进行分页
        PageHelper.startPage(pageNum,pageSize);
        List<VideoUserName> list = videoMapper.findALLVideoByClassify(videoClassify);
        return new PageInfo<>(list);
    }

    /**
     * 搜索视频
     * @param keyword 搜索关键字
     * @return 视频集合
     */
    @Override
    public PageInfo<VideoUserName> findSearchVideo(String keyword,Integer pageNum,Integer pageSize) {
        //使用分页插件进行分页
        PageHelper.startPage(pageNum,pageSize);
        List<VideoUserName> list = videoMapper.findSearchVideo(keyword);
        return new PageInfo<>(list);
    }

    public List<Video> findVideoByUserIdAndNominate(Integer videoUserId,Integer videoNominate){
        HashMap<String,Integer> hashMap=new HashMap<>();
        hashMap.put("videoUserId",videoUserId);
        hashMap.put("videoNominate",videoNominate);
      return videoMapper.findVideoByUserIdAndNominate(hashMap);
    }

}

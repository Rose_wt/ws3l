package com.ws3l.service.impl;

import com.ws3l.mapper.MemberMapper;
import com.ws3l.pojo.Member;
import com.ws3l.service.MemberServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberService implements MemberServices {
    @Autowired
    MemberMapper memberMapper;

    @Override
    public List<Member> showAll() {
        return memberMapper.showAll();
    }
}

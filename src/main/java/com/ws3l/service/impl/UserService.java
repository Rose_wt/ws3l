package com.ws3l.service.impl;

import com.ws3l.mapper.UserMapper;
import com.ws3l.pojo.User;
import com.ws3l.service.UserServices;
import com.ws3l.util.MDigest5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserServices {
    @Autowired
    private UserMapper userMapper=null;
    MDigest5 md5=new MDigest5();

    /**
     * 用户注册
     * @param userName 用户名
     * @param userPwd   用户密码
     * @return
     */
    @Override
    public User userLogin(String userName, String userPwd) {
       User user=userMapper.findUserByName(userName);

        String myUserPwd=md5.getMD5(userPwd);
       if(user!=null){
           if(user.getUserPassword().equals(myUserPwd)){
               return user;
           }
           else return null;
       }
       else return null;
    }

    @Override
    public User findUserByUserId(Integer id) {
      return userMapper.findUserByUserId(id);
    }
    public User findUserByName(String uname){
        return userMapper.findUserByName(uname);
    }
    /**
     * 用户注册
     * @param user
     * @return
     */
    @Override
    public int userSignUp(User user) {
//        System.out.println("userService:"+user);
//        System.out.println("userServiceGetName:"+user.getUserName());
       User findUser= userMapper.findUserByName(user.getUserName());
//        System.out.println("findUser:"+findUser);
      String oldPwd=  user.getUserPassword();

      String  newPwd= md5.getMD5(oldPwd);
      user.setUserPassword(newPwd);
       int flag=0;
       if (findUser!=null){
           //用户名已存在
           flag=1;
       }else {
           userMapper.addUser(user);
       }
        return flag;
    }

    /**
     * 根据邮箱修改密码
     * @param userEmail
     * @param userPassword
     * @return
     */
    @Override
    public int updateUserPwdByEmail(String userEmail, String userPassword) {
        String pwd=md5.getMD5(userPassword);
        int row=userMapper.updateUserPwdByEmail(userEmail,pwd);
        return row;
    }

    @Override
    public int updateUserByUser(User user) {
        return userMapper.updateUserByUser(user);
    }
}

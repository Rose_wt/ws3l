package com.ws3l.service.impl;

import com.ws3l.mapper.AdminMapper;
import com.ws3l.pojo.Admin;
import com.ws3l.pojo.Member;
import com.ws3l.service.AdminServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService implements AdminServices {
    @Autowired
    AdminMapper adminMapper = null;

    @Override
    public Admin AdminLogin(Integer Admin_id,String password) {
        Admin admin = adminMapper.AdminLogin(Admin_id,password);
        if (Admin_id!=null){
            if (admin.getAdminPassword().equals(password)){
                return admin;
            }else{
                return null;
            }
        }else {
            return null;
        }
    }
}

package com.ws3l.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class VideoUserName implements Serializable {

    private String userName;

    private Integer userId;

    private Integer videoId;

    private Integer videoCounts;

    private Integer videoTags;

    private String videoTitle;

    private String videoDuration;

    private String videoDescription;

    private String videoImage;

    private String videoMovieurls;

    private Integer videoClassify; //1为搞笑，2为舞蹈，3为科普，4为游戏

    private Integer videoNominate; //1为正常，2为正在审核视频，不展示，3为审核失败

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date videoCreateTime;

    private Integer videoUserId;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

    public Integer getVideoCounts() {
        return videoCounts;
    }

    public void setVideoCounts(Integer videoCounts) {
        this.videoCounts = videoCounts;
    }

    public Integer getVideoTags() {
        return videoTags;
    }

    public void setVideoTags(Integer videoTags) {
        this.videoTags = videoTags;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(String videoDuration) {
        this.videoDuration = videoDuration;
    }

    public String getVideoDescription() {
        return videoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        this.videoDescription = videoDescription;
    }

    public String getVideoImage() {
        return videoImage;
    }

    public void setVideoImage(String videoImage) {
        this.videoImage = videoImage;
    }

    public String getVideoMovieurls() {
        return videoMovieurls;
    }

    public void setVideoMovieurls(String videoMovieurls) {
        this.videoMovieurls = videoMovieurls;
    }

    public Integer getVideoClassify() {
        return videoClassify;
    }

    public void setVideoClassify(Integer videoClassify) {
        this.videoClassify = videoClassify;
    }

    public Integer getVideoNominate() {
        return videoNominate;
    }

    public void setVideoNominate(Integer videoNominate) {
        this.videoNominate = videoNominate;
    }

    public Date getVideoCreateTime() {
        return videoCreateTime;
    }

    public void setVideoCreateTime(Date videoCreateTime) {
        this.videoCreateTime = videoCreateTime;
    }

    public Integer getVideoUserId() {
        return videoUserId;
    }

    public void setVideoUserId(Integer videoUserId) {
        this.videoUserId = videoUserId;
    }

    @Override
    public String toString() {
        return "VideoUserName{" +
                "userName='" + userName + '\'' +
                ", userId=" + userId +
                ", videoId=" + videoId +
                ", videoCounts=" + videoCounts +
                ", videoTags=" + videoTags +
                ", videoTitle='" + videoTitle + '\'' +
                ", videoDuration='" + videoDuration + '\'' +
                ", videoDescription='" + videoDescription + '\'' +
                ", videoImage='" + videoImage + '\'' +
                ", videoMovieurls='" + videoMovieurls + '\'' +
                ", videoClassify=" + videoClassify +
                ", videoNominate=" + videoNominate +
                ", videoCreateTime=" + videoCreateTime +
                ", videoUserId=" + videoUserId +
                '}';
    }
}

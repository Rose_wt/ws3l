package com.ws3l.mapper;

import com.ws3l.pojo.Member;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MemberMapper {
    List<Member> showAll();
}

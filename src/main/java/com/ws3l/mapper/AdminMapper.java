package com.ws3l.mapper;

import com.ws3l.pojo.Admin;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdminMapper {
    public Admin AdminLogin(Integer Admin_id,String password);
}

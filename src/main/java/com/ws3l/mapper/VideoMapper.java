package com.ws3l.mapper;

import com.ws3l.model.VideoUserName;
import com.ws3l.pojo.Video;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

@Mapper
public interface VideoMapper {

    List<VideoUserName> findHotVideo();

    Video findVideoById(Integer videoId);

    List<VideoUserName> findVideoByClassify(@Param("classify") Integer classify,@Param("idList") List<Integer> idList);

    Integer addTags(Integer videoId);

    Integer addCounts(Integer videoId);

    Integer addVideo(Video video);

    List<VideoUserName> findHotVideoByClassify(Integer videoClassify, Integer videoId);

    List<VideoUserName> findALLVideoByClassify(Integer videoClassify);

    List<VideoUserName> findSearchVideo(String keyword);
    List<Video> findVideoByUserIdAndNominate(HashMap<String,Integer> hashMap);

}

package com.ws3l.mapper;

import com.ws3l.pojo.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {
    public User findUserByName(String userName);
    public User findUserByUserId(Integer id);
    public int addUser(User user);
    int updateUserPwdByEmail(String userEmail,String userPassword);
    int updateUserByUser(User user);
}

package com.ws3l.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MDigest5 {
    private String md5(String plainText) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plainText.getBytes());
            byte b[] = md.digest();
            int i;
            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            return buf.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
    /**
     * 返回MD5加密串(默认32位)
     * @param plainText 需要加密的字符串
     * @return 返回MD5加密串
     */
    public String getMD5(String plainText) {
        return md5(plainText);

    }
    /**
     * 返回MD5加密串
     * @param plainText 需要加密的字符串
     * @param subCount 返回加密串的位数（建议在16-32之间）
     * @return
     */
    public String getMD5(String plainText,int subCount) {
        String str = md5(plainText);
        if(subCount>=16 && subCount <32) {
            return str.substring(0, subCount);
        }
        else {
            return str;
        }
    }
//	public static void main(String agrs[]) {
//		MDigest5 md51 = new MDigest5();
//		System.out.println(md51.getMD5("123456"));
//		System.out.println(md51.getMD5("123456", 16));
//	}
}

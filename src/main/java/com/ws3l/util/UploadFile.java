package com.ws3l.util;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class UploadFile {
    public static final String shopImage = "shop";
    public static final String userImage = "user";
    public static final String videoImage = "video";
    public static String uploadFile(MultipartFile uploadFiles,String state) throws IOException {
        if (uploadFiles != null){
            //获取文件的上传名称
            String fileName = uploadFiles.getOriginalFilename();
            //截取其后缀格式
            assert fileName != null;
            String fileType = fileName.substring(fileName.lastIndexOf("."));
            //通过唯一的UUID重新命名
            String newFileName = UUID.randomUUID() + fileType;
            File directory = new File("").getCanonicalFile();//参数为空
            //获取项目路径
            String projectPath = directory.getCanonicalPath();
            //拼接绝对路径
            String savePath = projectPath + File.separator+"src"+File.separator+"main"+File.separator+"resources"+File.separator;
            //相对路径
            String relativePath;
            //判断文件属性 指定保存目录
            if (fileType.equals(".mp4") || fileType.equals(".webm") || fileType.equals(".ogg")){
                relativePath = "static/videoFile";
                savePath = savePath + relativePath + File.separator;
            }else if (state.equals(videoImage)){
                relativePath = "static/images/video";
                savePath = savePath + relativePath + File.separator;
            }else if (state.equals(shopImage)){
                relativePath = "static/images/shop";
                savePath = savePath + relativePath + File.separator;
            }else if (state.equals(userImage)){
                relativePath = "static/images/user";
                savePath = savePath + relativePath + File.separator;
            }else {
                return null;
            }
            File file = new File(savePath);
            //目录不存在则创建目录
            if (!file.exists()){
                file.mkdirs();
            }
            try {
                File transferSavePath = new File(savePath,newFileName);
                //另存文件
                uploadFiles.transferTo(transferSavePath);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            //返回存储相对路径
            return  relativePath + "/" + newFileName;
        }
        return null;
    }
}

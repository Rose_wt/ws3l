package com.ws3l.pojo;

public class Arrived {
    private Integer arrivedId;

    private String arrivedName;

    private String arrivedAddress;

    private String arrivedPhone;

    private Integer arrivedUserId;

    public Integer getArrivedId() {
        return arrivedId;
    }

    public void setArrivedId(Integer arrivedId) {
        this.arrivedId = arrivedId;
    }

    public String getArrivedName() {
        return arrivedName;
    }

    public void setArrivedName(String arrivedName) {
        this.arrivedName = arrivedName == null ? null : arrivedName.trim();
    }

    public String getArrivedAddress() {
        return arrivedAddress;
    }

    public void setArrivedAddress(String arrivedAddress) {
        this.arrivedAddress = arrivedAddress == null ? null : arrivedAddress.trim();
    }

    public String getArrivedPhone() {
        return arrivedPhone;
    }

    public void setArrivedPhone(String arrivedPhone) {
        this.arrivedPhone = arrivedPhone == null ? null : arrivedPhone.trim();
    }

    public Integer getArrivedUserId() {
        return arrivedUserId;
    }

    public void setArrivedUserId(Integer arrivedUserId) {
        this.arrivedUserId = arrivedUserId;
    }

    @Override
    public String toString() {
        return "Arrived{" +
                "arrivedId=" + arrivedId +
                ", arrivedName='" + arrivedName + '\'' +
                ", arrivedAddress='" + arrivedAddress + '\'' +
                ", arrivedPhone='" + arrivedPhone + '\'' +
                ", arrivedUserId=" + arrivedUserId +
                '}';
    }
}
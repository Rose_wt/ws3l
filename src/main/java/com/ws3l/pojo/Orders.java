package com.ws3l.pojo;

import java.util.Date;

public class Orders {
    private Integer ordersId;

    private String ordersUserId;

    private Integer ordersCommodityId;

    private Integer ordersDeliveryId;

    private Integer ordersArrivedId;

    private Date ordersTime;

    private Integer ordersState;

    public Integer getOrdersId() {
        return ordersId;
    }

    public void setOrdersId(Integer ordersId) {
        this.ordersId = ordersId;
    }

    public String getOrdersUserId() {
        return ordersUserId;
    }

    public void setOrdersUserId(String ordersUserId) {
        this.ordersUserId = ordersUserId == null ? null : ordersUserId.trim();
    }

    public Integer getOrdersCommodityId() {
        return ordersCommodityId;
    }

    public void setOrdersCommodityId(Integer ordersCommodityId) {
        this.ordersCommodityId = ordersCommodityId;
    }

    public Integer getOrdersDeliveryId() {
        return ordersDeliveryId;
    }

    public void setOrdersDeliveryId(Integer ordersDeliveryId) {
        this.ordersDeliveryId = ordersDeliveryId;
    }

    public Integer getOrdersArrivedId() {
        return ordersArrivedId;
    }

    public void setOrdersArrivedId(Integer ordersArrivedId) {
        this.ordersArrivedId = ordersArrivedId;
    }

    public Date getOrdersTime() {
        return ordersTime;
    }

    public void setOrdersTime(Date ordersTime) {
        this.ordersTime = ordersTime;
    }

    public Integer getOrdersState() {
        return ordersState;
    }

    public void setOrdersState(Integer ordersState) {
        this.ordersState = ordersState;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "ordersId=" + ordersId +
                ", ordersUserId='" + ordersUserId + '\'' +
                ", ordersCommodityId=" + ordersCommodityId +
                ", ordersDeliveryId=" + ordersDeliveryId +
                ", ordersArrivedId=" + ordersArrivedId +
                ", ordersTime=" + ordersTime +
                ", ordersState=" + ordersState +
                '}';
    }
}
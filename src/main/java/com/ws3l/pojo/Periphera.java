package com.ws3l.pojo;

public class Periphera {
    private Integer peripheraId;

    private String peripheraName;

    private String peripheraColor;

    private String peripheraSize;

    private Double peripheraPrice;

    private Double peripheraMemberPrice;

    private Integer peripheraStock;

    private Integer peripheraDeliveryId;

    private String peripheraRemark;

    public Integer getPeripheraId() {
        return peripheraId;
    }

    public void setPeripheraId(Integer peripheraId) {
        this.peripheraId = peripheraId;
    }

    public String getPeripheraName() {
        return peripheraName;
    }

    public void setPeripheraName(String peripheraName) {
        this.peripheraName = peripheraName == null ? null : peripheraName.trim();
    }

    public String getPeripheraColor() {
        return peripheraColor;
    }

    public void setPeripheraColor(String peripheraColor) {
        this.peripheraColor = peripheraColor == null ? null : peripheraColor.trim();
    }

    public String getPeripheraSize() {
        return peripheraSize;
    }

    public void setPeripheraSize(String peripheraSize) {
        this.peripheraSize = peripheraSize == null ? null : peripheraSize.trim();
    }

    public Double getPeripheraPrice() {
        return peripheraPrice;
    }

    public void setPeripheraPrice(Double peripheraPrice) {
        this.peripheraPrice = peripheraPrice;
    }

    public Double getPeripheraMemberPrice() {
        return peripheraMemberPrice;
    }

    public void setPeripheraMemberPrice(Double peripheraMemberPrice) {
        this.peripheraMemberPrice = peripheraMemberPrice;
    }

    public Integer getPeripheraStock() {
        return peripheraStock;
    }

    public void setPeripheraStock(Integer peripheraStock) {
        this.peripheraStock = peripheraStock;
    }

    public Integer getPeripheraDeliveryId() {
        return peripheraDeliveryId;
    }

    public void setPeripheraDeliveryId(Integer peripheraDeliveryId) {
        this.peripheraDeliveryId = peripheraDeliveryId;
    }

    public String getPeripheraRemark() {
        return peripheraRemark;
    }

    public void setPeripheraRemark(String peripheraRemark) {
        this.peripheraRemark = peripheraRemark == null ? null : peripheraRemark.trim();
    }

    @Override
    public String toString() {
        return "Periphera{" +
                "peripheraId=" + peripheraId +
                ", peripheraName='" + peripheraName + '\'' +
                ", peripheraColor='" + peripheraColor + '\'' +
                ", peripheraSize='" + peripheraSize + '\'' +
                ", peripheraPrice=" + peripheraPrice +
                ", peripheraMemberPrice=" + peripheraMemberPrice +
                ", peripheraStock=" + peripheraStock +
                ", peripheraDeliveryId=" + peripheraDeliveryId +
                ", peripheraRemark='" + peripheraRemark + '\'' +
                '}';
    }
}
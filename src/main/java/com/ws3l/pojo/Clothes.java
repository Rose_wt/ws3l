package com.ws3l.pojo;

public class Clothes {
    private Integer clothesId;

    private String clothesName;

    private String clothesColor;

    private String clothesSize;

    private Double clothesPrice;

    private Double clothesMemberPrice;

    private Integer clothesStock;

    private Integer clothesDeliveryId;

    private String clothesRemark;

    public Integer getClothesId() {
        return clothesId;
    }

    public void setClothesId(Integer clothesId) {
        this.clothesId = clothesId;
    }

    public String getClothesName() {
        return clothesName;
    }

    public void setClothesName(String clothesName) {
        this.clothesName = clothesName == null ? null : clothesName.trim();
    }

    public String getClothesColor() {
        return clothesColor;
    }

    public void setClothesColor(String clothesColor) {
        this.clothesColor = clothesColor == null ? null : clothesColor.trim();
    }

    public String getClothesSize() {
        return clothesSize;
    }

    public void setClothesSize(String clothesSize) {
        this.clothesSize = clothesSize == null ? null : clothesSize.trim();
    }

    public Double getClothesPrice() {
        return clothesPrice;
    }

    public void setClothesPrice(Double clothesPrice) {
        this.clothesPrice = clothesPrice;
    }

    public Double getClothesMemberPrice() {
        return clothesMemberPrice;
    }

    public void setClothesMemberPrice(Double clothesMemberPrice) {
        this.clothesMemberPrice = clothesMemberPrice;
    }

    public Integer getClothesStock() {
        return clothesStock;
    }

    public void setClothesStock(Integer clothesStock) {
        this.clothesStock = clothesStock;
    }

    public Integer getClothesDeliveryId() {
        return clothesDeliveryId;
    }

    public void setClothesDeliveryId(Integer clothesDeliveryId) {
        this.clothesDeliveryId = clothesDeliveryId;
    }

    public String getClothesRemark() {
        return clothesRemark;
    }

    public void setClothesRemark(String clothesRemark) {
        this.clothesRemark = clothesRemark == null ? null : clothesRemark.trim();
    }

    @Override
    public String toString() {
        return "Clothes{" +
                "clothesId=" + clothesId +
                ", clothesName='" + clothesName + '\'' +
                ", clothesColor='" + clothesColor + '\'' +
                ", clothesSize='" + clothesSize + '\'' +
                ", clothesPrice=" + clothesPrice +
                ", clothesMemberPrice=" + clothesMemberPrice +
                ", clothesStock=" + clothesStock +
                ", clothesDeliveryId=" + clothesDeliveryId +
                ", clothesRemark='" + clothesRemark + '\'' +
                '}';
    }
}
package com.ws3l.pojo;

public class Shoes {
    private Integer shoesId;

    private String shoesName;

    private String shoesColor;

    private Integer shoesSize;

    private Double shoesPrice;

    private Double shoesMemberPrice;

    private Integer shoesStock;

    private Integer shoesDeliveryId;

    private String shoesRemark;

    public Integer getShoesId() {
        return shoesId;
    }

    public void setShoesId(Integer shoesId) {
        this.shoesId = shoesId;
    }

    public String getShoesName() {
        return shoesName;
    }

    public void setShoesName(String shoesName) {
        this.shoesName = shoesName == null ? null : shoesName.trim();
    }

    public String getShoesColor() {
        return shoesColor;
    }

    public void setShoesColor(String shoesColor) {
        this.shoesColor = shoesColor == null ? null : shoesColor.trim();
    }

    public Integer getShoesSize() {
        return shoesSize;
    }

    public void setShoesSize(Integer shoesSize) {
        this.shoesSize = shoesSize;
    }

    public Double getShoesPrice() {
        return shoesPrice;
    }

    public void setShoesPrice(Double shoesPrice) {
        this.shoesPrice = shoesPrice;
    }

    public Double getShoesMemberPrice() {
        return shoesMemberPrice;
    }

    public void setShoesMemberPrice(Double shoesMemberPrice) {
        this.shoesMemberPrice = shoesMemberPrice;
    }

    public Integer getShoesStock() {
        return shoesStock;
    }

    public void setShoesStock(Integer shoesStock) {
        this.shoesStock = shoesStock;
    }

    public Integer getShoesDeliveryId() {
        return shoesDeliveryId;
    }

    public void setShoesDeliveryId(Integer shoesDeliveryId) {
        this.shoesDeliveryId = shoesDeliveryId;
    }

    public String getShoesRemark() {
        return shoesRemark;
    }

    public void setShoesRemark(String shoesRemark) {
        this.shoesRemark = shoesRemark == null ? null : shoesRemark.trim();
    }

    @Override
    public String toString() {
        return "Shoes{" +
                "shoesId=" + shoesId +
                ", shoesName='" + shoesName + '\'' +
                ", shoesColor='" + shoesColor + '\'' +
                ", shoesSize=" + shoesSize +
                ", shoesPrice=" + shoesPrice +
                ", shoesMemberPrice=" + shoesMemberPrice +
                ", shoesStock=" + shoesStock +
                ", shoesDeliveryId=" + shoesDeliveryId +
                ", shoesRemark='" + shoesRemark + '\'' +
                '}';
    }
}
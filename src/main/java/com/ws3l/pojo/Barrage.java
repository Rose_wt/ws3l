package com.ws3l.pojo;

public class Barrage {
    private Integer barrageId;

    private String barrageTime;

    private String barrageColor;

    private Integer barrageUserId;

    private Integer barrageVideoId;

    public Integer getBarrageId() {
        return barrageId;
    }

    public void setBarrageId(Integer barrageId) {
        this.barrageId = barrageId;
    }

    public String getBarrageTime() {
        return barrageTime;
    }

    public void setBarrageTime(String barrageTime) {
        this.barrageTime = barrageTime == null ? null : barrageTime.trim();
    }

    public String getBarrageColor() {
        return barrageColor;
    }

    public void setBarrageColor(String barrageColor) {
        this.barrageColor = barrageColor == null ? null : barrageColor.trim();
    }

    public Integer getBarrageUserId() {
        return barrageUserId;
    }

    public void setBarrageUserId(Integer barrageUserId) {
        this.barrageUserId = barrageUserId;
    }

    public Integer getBarrageVideoId() {
        return barrageVideoId;
    }

    public void setBarrageVideoId(Integer barrageVideoId) {
        this.barrageVideoId = barrageVideoId;
    }

    @Override
    public String toString() {
        return "Barrage{" +
                "barrageId=" + barrageId +
                ", barrageTime='" + barrageTime + '\'' +
                ", barrageColor='" + barrageColor + '\'' +
                ", barrageUserId=" + barrageUserId +
                ", barrageVideoId=" + barrageVideoId +
                '}';
    }
}
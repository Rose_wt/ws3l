package com.ws3l.pojo;

public class Garagekgits {
    private Integer garagekgitsId;

    private String garagekgitsName;

    private String garagekgitsColor;

    private String garagekgitsSize;

    private Double garagekgitsPrice;

    private Double garagekgitsMemberPrice;

    private Integer garagekgitsStock;

    private Integer garagekgitsDeliveryId;

    private String garagekgitsRemark;

    public Integer getGaragekgitsId() {
        return garagekgitsId;
    }

    public void setGaragekgitsId(Integer garagekgitsId) {
        this.garagekgitsId = garagekgitsId;
    }

    public String getGaragekgitsName() {
        return garagekgitsName;
    }

    public void setGaragekgitsName(String garagekgitsName) {
        this.garagekgitsName = garagekgitsName == null ? null : garagekgitsName.trim();
    }

    public String getGaragekgitsColor() {
        return garagekgitsColor;
    }

    public void setGaragekgitsColor(String garagekgitsColor) {
        this.garagekgitsColor = garagekgitsColor == null ? null : garagekgitsColor.trim();
    }

    public String getGaragekgitsSize() {
        return garagekgitsSize;
    }

    public void setGaragekgitsSize(String garagekgitsSize) {
        this.garagekgitsSize = garagekgitsSize == null ? null : garagekgitsSize.trim();
    }

    public Double getGaragekgitsPrice() {
        return garagekgitsPrice;
    }

    public void setGaragekgitsPrice(Double garagekgitsPrice) {
        this.garagekgitsPrice = garagekgitsPrice;
    }

    public Double getGaragekgitsMemberPrice() {
        return garagekgitsMemberPrice;
    }

    public void setGaragekgitsMemberPrice(Double garagekgitsMemberPrice) {
        this.garagekgitsMemberPrice = garagekgitsMemberPrice;
    }

    public Integer getGaragekgitsStock() {
        return garagekgitsStock;
    }

    public void setGaragekgitsStock(Integer garagekgitsStock) {
        this.garagekgitsStock = garagekgitsStock;
    }

    public Integer getGaragekgitsDeliveryId() {
        return garagekgitsDeliveryId;
    }

    public void setGaragekgitsDeliveryId(Integer garagekgitsDeliveryId) {
        this.garagekgitsDeliveryId = garagekgitsDeliveryId;
    }

    public String getGaragekgitsRemark() {
        return garagekgitsRemark;
    }

    public void setGaragekgitsRemark(String garagekgitsRemark) {
        this.garagekgitsRemark = garagekgitsRemark == null ? null : garagekgitsRemark.trim();
    }

    @Override
    public String toString() {
        return "Garagekgits{" +
                "garagekgitsId=" + garagekgitsId +
                ", garagekgitsName='" + garagekgitsName + '\'' +
                ", garagekgitsColor='" + garagekgitsColor + '\'' +
                ", garagekgitsSize='" + garagekgitsSize + '\'' +
                ", garagekgitsPrice=" + garagekgitsPrice +
                ", garagekgitsMemberPrice=" + garagekgitsMemberPrice +
                ", garagekgitsStock=" + garagekgitsStock +
                ", garagekgitsDeliveryId=" + garagekgitsDeliveryId +
                ", garagekgitsRemark='" + garagekgitsRemark + '\'' +
                '}';
    }
}
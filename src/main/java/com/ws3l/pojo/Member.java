package com.ws3l.pojo;

import java.util.Date;

public class Member {
    private Integer memberId;

    private Date memberStartTime;

    private Date memberEndTime;

    private Double memberPayMoney;

    private String memberPayTrench;

    private Integer memberUserId;

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Date getMemberStartTime() {
        return memberStartTime;
    }

    public void setMemberStartTime(Date memberStartTime) {
        this.memberStartTime = memberStartTime;
    }

    public Date getMemberEndTime() {
        return memberEndTime;
    }

    public void setMemberEndTime(Date memberEndTime) {
        this.memberEndTime = memberEndTime;
    }

    public Double getMemberPayMoney() {
        return memberPayMoney;
    }

    public void setMemberPayMoney(Double memberPayMoney) {
        this.memberPayMoney = memberPayMoney;
    }

    public String getMemberPayTrench() {
        return memberPayTrench;
    }

    public void setMemberPayTrench(String memberPayTrench) {
        this.memberPayTrench = memberPayTrench == null ? null : memberPayTrench.trim();
    }

    public Integer getMemberUserId() {
        return memberUserId;
    }

    public void setMemberUserId(Integer memberUserId) {
        this.memberUserId = memberUserId;
    }

    @Override
    public String toString() {
        return "Member{" +
                "memberId=" + memberId +
                ", memberStartTime=" + memberStartTime +
                ", memberEndTime=" + memberEndTime +
                ", memberPayMoney=" + memberPayMoney +
                ", memberPayTrench='" + memberPayTrench + '\'' +
                ", memberUserId=" + memberUserId +
                '}';
    }
}
package com.ws3l.pojo;

import java.util.Date;

public class Comment {
    private Integer commentId;

    private Integer commentVideoId;

    private Integer commentUserId;

    private String commentContent;

    private Date commentTime;

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public Integer getCommentVideoId() {
        return commentVideoId;
    }

    public void setCommentVideoId(Integer commentVideoId) {
        this.commentVideoId = commentVideoId;
    }

    public Integer getCommentUserId() {
        return commentUserId;
    }

    public void setCommentUserId(Integer commentUserId) {
        this.commentUserId = commentUserId;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent == null ? null : commentContent.trim();
    }

    public Date getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(Date commentTime) {
        this.commentTime = commentTime;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "commentId=" + commentId +
                ", commentVideoId=" + commentVideoId +
                ", commentUserId=" + commentUserId +
                ", commentContent='" + commentContent + '\'' +
                ", commentTime=" + commentTime +
                '}';
    }
}
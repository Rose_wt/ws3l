package com.ws3l.pojo;

public class Report {
    private Integer reportId;

    private Integer reportType;

    private String reportReason;

    private String reportDetail;

    private Integer reportState;

    private Integer reportUserId;

    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }

    public Integer getReportType() {
        return reportType;
    }

    public void setReportType(Integer reportType) {
        this.reportType = reportType;
    }

    public String getReportReason() {
        return reportReason;
    }

    public void setReportReason(String reportReason) {
        this.reportReason = reportReason == null ? null : reportReason.trim();
    }

    public String getReportDetail() {
        return reportDetail;
    }

    public void setReportDetail(String reportDetail) {
        this.reportDetail = reportDetail == null ? null : reportDetail.trim();
    }

    public Integer getReportState() {
        return reportState;
    }

    public void setReportState(Integer reportState) {
        this.reportState = reportState;
    }

    public Integer getReportUserId() {
        return reportUserId;
    }

    public void setReportUserId(Integer reportUserId) {
        this.reportUserId = reportUserId;
    }

    @Override
    public String toString() {
        return "Report{" +
                "reportId=" + reportId +
                ", reportType=" + reportType +
                ", reportReason='" + reportReason + '\'' +
                ", reportDetail='" + reportDetail + '\'' +
                ", reportState=" + reportState +
                ", reportUserId=" + reportUserId +
                '}';
    }
}
package com.ws3l.controller;

import com.github.pagehelper.PageInfo;
import com.ws3l.model.VideoUserName;
import com.ws3l.pojo.User;
import com.ws3l.pojo.Video;
import com.ws3l.service.impl.UserService;
import com.ws3l.service.impl.VideoService;
import com.ws3l.util.UploadFile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping("/video")
public class VideoController {
    @Autowired
    VideoService videoService;
    @Autowired
    UserService userService;

    //首页
    @RequestMapping("/videoIndex")
    public String videoIndex(Model model) {
        //查询热门视频
        List<VideoUserName> hotList = videoService.findHotVideo();
        //封装热门视频id
        List<Integer> idList = new ArrayList<>();
        for (int i = 0; i < hotList.size(); i++) {
            idList.add(hotList.get(i).getVideoId());
        }
        //查询分类推荐视频
        for (int i = 1; i < 5; i++) {
            List<VideoUserName> list = videoService.findVideoByClassify(i, idList);
            model.addAttribute("list" + i, list);
        }
        model.addAttribute("hostList", hotList);

        return "/video/index";
    }

    //进入投稿视频页面
    @RequestMapping("/toUploadVideo")
    public String uploadVideo() {
        return "/video/Upload_Video";
    }

    //视频详情
    @RequestMapping("/singleVideo/{id}")
    public String singleVideo(@PathVariable("id") Integer videoId, Model model) {
        if (videoId == null) {
            return "/video/index";
        }
        //视频详情
        Video video = videoService.findVideoById(videoId);
        User user = userService.findUserByUserId(video.getVideoUserId());
        model.addAttribute("video", video);
        model.addAttribute("userVideo", user);
        //热门视频推荐
        List<VideoUserName> hotList = videoService.findHotVideoByClassify(video.getVideoClassify(), videoId);
        model.addAttribute("hotList", hotList);
        return "/video/single_video_page";
    }

    //点赞
    @RequestMapping("/addTags/{id}")
    @ResponseBody
    public Map<String, String> addTags(@PathVariable("id") Integer videoId) {
        Map<String, String> map = new HashMap<>();
        if (videoId == null) {
            map.put("data", "param error");
            return map;
        }
        if (videoService.addTags(videoId) > 0) {
            map.put("data", "success");
        } else {
            map.put("data", "fail");
        }
        return map;
    }

    //增加播放量
    @RequestMapping("/addCounts/{id}")
    @ResponseBody
    public Map<String, String> addCounts(@PathVariable("id") Integer videoId) {
        Map<String, String> map = new HashMap<>();
        if (videoId == null) {
            map.put("data", "param error");
            return map;
        }
        if (videoService.addCounts(videoId) > 0) {
            map.put("data", "success");
        } else {
            map.put("data", "fail");
        }
        return map;
    }

    //获取上传的视频
    @RequestMapping("/uploadVideo")
    @ResponseBody
    public Map<String, String> uploadVideo(@RequestParam("videoFile") MultipartFile video, HttpSession session) {
        Map<String, String> map = new HashMap<>();
        if (video.isEmpty()) {
            map.put("data", "fail");
            return map;
        }
        String videoUrl = null;
        try {
            videoUrl = UploadFile.uploadFile(video, UploadFile.videoImage);
            if (videoUrl != null) {
                session.setAttribute("videoUrl", videoUrl);
                map.put("data", "success");
            } else {
                map.put("data", "fail");
            }
        } catch (IOException e) {
            map.put("data", "fail");
            e.printStackTrace();
        }
//        System.out.println(session.getAttribute("videoUrl"));
        return map;
    }

    //获取上传的视频其他信息
    @RequestMapping("/uploadVideoMessage")
    public String uploadVideoMessage(@RequestParam("videoImage") MultipartFile videoImage, String videoTitle,
                                     String videoDescription, String videoClassify, String videoDuration,
                                     String videoUserId, HttpSession session) {
        //验证视频地址
        String videoUrl = (String) session.getAttribute("videoUrl");
        if (videoUrl == null) {
            return "/video/Upload_Video";
        } else {
            session.removeAttribute("videoUrl");
        }
        try {
            //验证视频封面
            String videoImageUrl = UploadFile.uploadFile(videoImage, UploadFile.videoImage);
            if (videoImage == null) {
                return "/video/Upload_Video";
            }
            Video video = new Video();
            video.setVideoCounts(0);
            video.setVideoTags(0);
            video.setVideoDuration(videoDuration);
            video.setVideoTitle(videoTitle);
            video.setVideoDescription(videoDescription);
            video.setVideoMovieurls(videoUrl);
            video.setVideoClassify(Integer.parseInt(videoClassify));
            video.setVideoNominate(2);
            video.setVideoCreateTime(new Date());
            video.setVideoUserId(Integer.parseInt(videoUserId));
            video.setVideoImage(videoImageUrl);
            System.out.println(video);
            if (videoService.addVideo(video) > 0) {
                return "/video/upload_video_success";
            } else {
                return "/video/upload_video_error";
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "/video/Upload_Video";
        }
    }

    //视频分类分页展示页面
    @RequestMapping("showVideoClassify/{classify}/{pageNum}")
    public String showVideoClassify(@PathVariable("classify") Integer videoClassify,
                                    @PathVariable("pageNum") Integer pageNum, Model model) {
        if (videoClassify == null || pageNum == null) {
            return "/video/index";
        }
        PageInfo<VideoUserName> pageInfo = videoService.findALLVideoByClassify(videoClassify, pageNum, 16);
        model.addAttribute("message", "该分类下本站共有");
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("classify", videoClassify);
        model.addAttribute("showTag",1);
        return "/video/Searched_Videos_Page";
    }

    //搜索视频
    @RequestMapping("/searchVideo")
    public String searchVideo(String keyword, Integer pageNum, Model model) {
        if (keyword == null) {
            return "/video/Searched_Videos_Page";
        }
        PageInfo<VideoUserName> pageInfo = videoService.findSearchVideo(keyword, pageNum, 8);
        model.addAttribute("message", "本次共查询到");
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("keyword", keyword);
        //设置分页栏的显示
        model.addAttribute("showTag",2);
        return "/video/Searched_Videos_Page";
    }
}

package com.ws3l.controller;

import com.ws3l.service.impl.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/member")
public class MemberController {
    @Autowired
    MemberService memberService;
    @RequestMapping("/memberIndex")
    public String showAll(Model model){
//        model.addAttribute("memberList",memberService.showAll());
        return "Members opened/index";
    }
}

package com.ws3l.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/backstage")
public class BackstageController {
    @RequestMapping("/toindex")
    public String toindex(){
        return "backstage/index";
    }

    @RequestMapping("/home")
    public String home(){
        return "backstage/home";
    }
}

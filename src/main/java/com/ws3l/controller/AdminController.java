package com.ws3l.controller;


import com.ws3l.pojo.Admin;
import com.ws3l.pojo.User;
import com.ws3l.service.impl.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("admin")
public class AdminController {
    @Autowired
    AdminService adminService = null;

    @RequestMapping("login")
    public String adminLogin(){
        return "Admin/login";
    }

    @RequestMapping("doLogin")
    @ResponseBody
    public Admin doAdminLogin(Integer Admin_id, String password, HttpSession session){
        System.out.println(Admin_id+","+password);
        Admin admin=adminService.AdminLogin(Admin_id,password);
        System.out.println(admin);
        if (admin!=null){
            session.setAttribute("admin",admin);
        }
        return admin;
    }
}

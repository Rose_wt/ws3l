package com.ws3l.controller;

import com.ws3l.pojo.User;
import com.ws3l.pojo.Video;
import com.ws3l.service.impl.EmailService;
import com.ws3l.service.impl.UserService;
import com.ws3l.service.impl.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RequestMapping("user")
@Controller
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    EmailService emailService;
    @Autowired
    VideoService videoService;
    int code;


    @RequestMapping("login")
    public String userLogin(){
        return "video/login";
    }

    /**
     * 用户登录
     * @param username
     * @param password
     * @param session
     * @return
     */
    @RequestMapping("/dologin")
    @ResponseBody
    public User doUserLogin(String username, String password, HttpSession session){
//        System.out.println(username+","+password);
        User user=userService.userLogin(username,password);
//        System.out.println(user);
        if (user!=null){
            session.setAttribute("user",user);
        }
        return user;
    }
    @RequestMapping("/signup")
    public String signUp(){
        return "video/signup";
    }

    /**
     * 用户注册
     * @param userName
     * @param userPasswd
     * @param userTel
     * @param userEmail
     * @param emailCode
     * @return
     */
    @RequestMapping("/doSignUp")
    @ResponseBody
    public int doSignUp(String userName,String userPasswd,String userTel,String userEmail,String emailCode){
        User user=new User();
        user.setUserName(userName);
        user.setUserPassword(userPasswd);
        user.setUserTel(userTel);
        user.setUserEmail(userEmail);
        user.setUserIcon("static/images/user/user1.jpg");
       int row= userService.userSignUp(user);
       if (Integer.parseInt(emailCode)!=code){
           //验证码不匹配
           row=2;
       }
        return row;
    }

    /**
     * 发送邮箱验证码
     * @param userEmail
     * @param emailCodeSession
     * @return
     */
    @RequestMapping("/sendEmailCode")
    @ResponseBody
    public int sendEmailCode(String userEmail,HttpSession emailCodeSession){
        System.out.println(userEmail);
        code= (int)Math.round((Math.random()+1) * 1000);
        emailCodeSession.setAttribute("emailCode",code);
        //五分钟有效
        emailCodeSession.setMaxInactiveInterval(5*60);
        System.out.println(code);
//        String to="326125768@qq.com";
        String to=userEmail;
        String subject="五十三里视频网";
        String text="尊敬的用户，欢迎使用五十三里视频网，您的验证码为："+code+",有效期为5分钟，请及时输入";
        emailService.sendEmail(to, subject, text);
        return code;
    }
    /**
     * 跳转到忘记密码页面
     * @return
     */
    @RequestMapping("/forgetUserPwd")
    public String forgetUserPwd(){
        return "video/forget_userpwd";
    }

    /**
     * 处理忘记密码页面的数据
     * @param userPasswd
     * @param userEmail
     * @param emailCode
     * @return
     */
    @RequestMapping("/doForgetUserPwd")
    @ResponseBody
    public int doForgetUserPwd(String userPasswd,String userEmail,String emailCode){
        int row=0;
        if (Integer.parseInt(emailCode)!=code){
            //验证码不正确
            row=2;
        }
        row=userService.updateUserPwdByEmail(userEmail,userPasswd);
        return row;
    }

    /**
     * @return 跳转到前台个人中心页面
     */
    @RequestMapping("/userAccountPage")
    public String userAccountPage(Model model, HttpServletRequest request){
        Object oldUser=request.getSession().getAttribute("user");
        System.out.println("olduser:"+oldUser);
        User newUser=(User) oldUser;
        System.out.println(newUser.getUserId());
        //个人信息
        model.addAttribute("user",newUser);
        //视频信息
        //审核通过的视频
      List<Video> successVideoList= videoService.findVideoByUserIdAndNominate(newUser.getUserId(),1);
        //正在审核的视频
        List<Video> auditingVideoList= videoService.findVideoByUserIdAndNominate(newUser.getUserId(),2);
        //审核失败的视频
        List<Video> failVideoList= videoService.findVideoByUserIdAndNominate(newUser.getUserId(),3);

        System.out.println("successVideoList:"+successVideoList);
        model.addAttribute("successVideoList",successVideoList);
        model.addAttribute("auditingVideoList",auditingVideoList);
        model.addAttribute("failVideoList",failVideoList);
        return "video/User_Account_Page";
    }
    @RequestMapping("/doUserAccountEdit")
    @ResponseBody
    public int doUserAccountEdit(String userId,String userName,String userTel,String userEmail){
        System.out.println("userName"+userName);
        System.out.println("userTel"+userTel);
        System.out.println("userEmail"+userEmail);
       User user= userService.findUserByUserId(Integer.parseInt(userId));
        user.setUserName(userName);
        user.setUserTel(userTel);
        user.setUserEmail(userEmail);
       int row= userService.updateUserByUser(user);
        return row;
    }

}

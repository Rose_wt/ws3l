package com.ws3l.controller;

public class AlipayController {
    // 商户appid
    public static String APPID = "2021000118662733";
    // 私钥 pkcs8格式的
    public static String RSA_PRIVATE_KEY = "https://openapi.alipaydev.com/gateway.do";
    // 服务器异步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://商户网关地址/alipay.trade.wap.pay-JAVA-UTF-8/notify_url.jsp";
    // 页面跳转同步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问 商户可以自定义同步跳转地址
    public static String return_url = "http://商户网关地址/alipay.trade.wap.pay-JAVA-UTF-8/return_url.jsp";
    // 请求网关地址
    public static String URL = "https://openapi.alipay.com/gateway.do";
    // 编码
    public static String CHARSET = "UTF-8";
    // 返回格式
    public static String FORMAT = "json";
    // 支付宝公钥
    public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsSeK4aQNpvgFT/7EIOF2PPqGYSt0DBdwGo5ZHcCpGr+EU0gOMaxvku94bGFYUV6Kfi3u/CG3O8yXeH/UYKoi5XdahaIOWslfwDzfbyApABGWw8FFttldszxLcVi/QMKIuZ3sJxBULxBsy/1/cDk1iQjCjPagIdf36w4Hsva0vp3Djrs499M/fbJxyM3fnBsjidpcEU963Xd44+8cnN4u+uNAepmW+WESf4zTvW60pm4DlIOvh2QWNEAMSBbADBty1A8Hli1RaXGxqIacFOppRmEEazTVvDltjLG2Piqiw/OvhefFqIOhac/UgPPBvxjtrysRY8f3kdnzjvI1W1ACOwIDAQAB";
    // RSA2
    public static String SIGNTYPE = "RSA2";
}


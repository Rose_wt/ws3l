package com.ws3l;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;


@SpringBootApplication
//开启缓存
@EnableCaching
public class Ws3lApplication {
    public static void main(String[] args) {
        SpringApplication.run(Ws3lApplication.class,args);
    }
}
